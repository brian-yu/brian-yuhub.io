var end = new Date("June 23, 2016 14:00:00:00").getTime();
var start = new Date("September 8, 2015 08:40:00:00").getTime();
var total = end - start;
var days, hours, minutes, seconds, milliseconds;
var percentage = 0.000;
var inter;
var countdown = document.getElementById("countdown");
run();


function run() {
  inter = setInterval(timer, 1);
}

function timer(){
  var now = Date.now();
  var msLeft = (end - now);
  var seconds_left = (end - now) / 1000;
  var msPassed = (now - start);
  var percentage = msPassed / total;
  percentage *= 100;
  percentage = percentage.toFixed(8);

  days = parseInt(seconds_left / 86400);
  seconds_left = seconds_left % 86400;

  hours = parseInt(seconds_left / 3600);
  seconds_left = seconds_left % 3600;

  minutes = parseInt(seconds_left / 60);
  seconds = parseInt(seconds_left % 60);

  countdown.innerHTML = "School is " + percentage + "% Complete";
  counter.innerHTML = days + "d, " + hours + "h, "+ minutes + "m, " + seconds + "s";
  $('.progress').css('width', percentage+'%');
  $('.progress').attr('aria-valuenow', percentage)
  console.log($('.progress').css('width'));

}
  // document.getElementById("countdown").aria-valuenow = percentage;
  // document.getElementById("countdown").style.width = percentage;
  // time.innerHTML = now;
