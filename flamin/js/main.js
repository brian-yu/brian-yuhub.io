$( "#starttext").click(function() {
  var start = new Date();
  $("#starttext").remove();
  $( ".wall").mouseenter(function() {
    var time = new Date()-start;
    $("#result").append("<p>Failed.</p>");
  });
  $( ".path").mouseleave(function() {
    $(this).css("background-color", "yellow");
  });
  $( "#end").mouseenter(function() {
    var time = new Date()-start;
    $("#result").append("<p>Won!</p>");
  });
});
$( ".path").mouseleave(function() {
  $(this).css("background-color", "yellow");
});
